# Gogo Space

Small microservice responsible for collecting image URLs from the open NASA's APOD API.

## How to use?

### Option 1: Compile and run from source

**Prerequisites**
- Go >= 1.17

1. ``git clone https://gitlab.com/Kichiyaki/rgf3awqgr29nb0fwchmgtkftqq.git``
2. ``cd rgf3awqgr29nb0fwchmgtkftqq``
3. ``go run ./cmd/httpserver/main.go`` or ``go build -o httpserver ./cmd/httpserver/main.go`` and then ``./httpserver``

### Option 2: Docker

**Prerequisites**
- Docker

1. ``git clone https://gitlab.com/Kichiyaki/rgf3awqgr29nb0fwchmgtkftqq.git``
2. ``cd rgf3awqgr29nb0fwchmgtkftqq``
3. ``docker build -t httpserver:latest -f ./build/httpserver/Dockerfile .``
4. ``docker run -p 8080:8080 httpserver:latest``

## Configuration

The app is configurable via environment variables.
```dotenv
API_KEY=DEMO_KEY # api_key used for NASA API
CONCURRENT_REQUESTS=5 # limit of concurrent requests to NASA API
PORT=8080 # a port the http server is running on
```
