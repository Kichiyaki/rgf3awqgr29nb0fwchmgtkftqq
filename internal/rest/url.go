package rest

import (
	"context"
	"net/http"

	"gitlab.com/Kichiyaki/rgf3awqgr29nb0fwchmgtkftqq/internal"
)

type URLService interface {
	CollectPictures(ctx context.Context, dates internal.Dates) ([]string, error)
}

type URLHandler struct {
	svc URLService
}

func NewURLHandler(svc URLService) *URLHandler {
	return &URLHandler{
		svc: svc,
	}
}

func (u *URLHandler) Register(mux *http.ServeMux) {
	mux.HandleFunc("/pictures", u.getPictures)
}

type getPicturesResponse struct {
	URLs []string `json:"urls"`
}

func (u *URLHandler) getPictures(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		renderErrorResponse(w, internal.NewError(internal.ErrorCodeMethodNotAllowed, "method not allowed"), "")
		return
	}

	q := r.URL.Query()
	dates, err := internal.NewDates(q.Get("from"), q.Get("to"))
	if err != nil {
		renderErrorResponse(w, err, err.Error())
		return
	}

	urls, err := u.svc.CollectPictures(r.Context(), dates)
	if err != nil {
		renderErrorResponse(w, err, "")
		return
	}

	renderResponse(w, http.StatusOK, &getPicturesResponse{URLs: urls})
}
