package rest

import (
	"encoding/json"
	"net/http"

	"gitlab.com/Kichiyaki/rgf3awqgr29nb0fwchmgtkftqq/internal"
)

type ErrorResponse struct {
	Error string `json:"error"`
}

// renderErrorResponse renders error response.
// When msg == "", ErrorCode.String is used as the error message.
func renderErrorResponse(w http.ResponseWriter, err error, msg string) {
	code := internal.GetErrorCode(err)

	if msg == "" {
		msg = code.String()
	}

	resp := ErrorResponse{Error: msg}

	renderResponse(w, errorCodeToHTTPStatus(code), resp)
}

func errorCodeToHTTPStatus(code internal.ErrorCode) int {
	switch code {
	case internal.ErrorCodeBadArgument:
		return http.StatusBadRequest
	case internal.ErrorCodeMethodNotAllowed:
		return http.StatusMethodNotAllowed
	case internal.ErrorCodeTooManyRequests:
		return http.StatusTooManyRequests
	case internal.ErrorCodeUnknown:
		fallthrough
	default:
		return http.StatusInternalServerError
	}
}

func renderResponse(w http.ResponseWriter, status int, res interface{}) {
	w.Header().Set("Content-Type", "application/json")

	content, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(status)

	if _, err := w.Write(content); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
}
