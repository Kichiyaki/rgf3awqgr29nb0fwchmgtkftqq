package service_test

import (
	"context"
	"testing"
	"time"

	"gitlab.com/Kichiyaki/rgf3awqgr29nb0fwchmgtkftqq/internal"
	"gitlab.com/Kichiyaki/rgf3awqgr29nb0fwchmgtkftqq/internal/service"
)

type mockedPictureCollector struct {
	CollectFn func(ctx context.Context, t time.Time) (string, error)
}

func (m *mockedPictureCollector) Collect(ctx context.Context, t time.Time) (string, error) {
	if m.CollectFn == nil {
		return "", nil
	}

	return m.CollectFn(ctx, t)
}

func TestURL_CollectPictures(t *testing.T) {
	t.Parallel()

	t.Run("OK", func(t *testing.T) {
		t.Parallel()

		collector := &mockedPictureCollector{
			CollectFn: func(ctx context.Context, t time.Time) (string, error) {
				return t.Format(time.RFC3339) + ".png", nil
			},
		}

		layout := "2006-01-02"
		dates, err := internal.NewDates(time.Now().Format(layout), time.Now().Add(24*time.Hour*33).Format(layout))
		if err != nil {
			t.Errorf("expected nil, got %v", err)
			return
		}

		days := int(dates.To().Sub(dates.From()).Hours()/24) + 1

		urls, err := service.NewURL(collector).CollectPictures(context.Background(), dates)
		if err != nil {
			t.Errorf("expected nil, got %v", err)
		}

		if len(urls) != days {
			t.Errorf("expected different length, got %d, expected %d", len(urls), days)
		}

		// urls should be sorted
		for i, url := range urls {
			ti, err := time.Parse(time.RFC3339+".png", url)
			if err != nil {
				t.Errorf("expected nil, got %v", err)
				break
			}

			day := int(ti.Sub(dates.From()) / 24 / time.Hour)

			if day != i {
				t.Errorf("incorrect URL order, url '%s' was supposed to be %d, but is %d", url, i, day)
			}
		}
	})
}
