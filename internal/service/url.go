package service

import (
	"context"
	"sync"
	"time"

	"gitlab.com/Kichiyaki/rgf3awqgr29nb0fwchmgtkftqq/internal"
)

type PictureCollector interface {
	Collect(ctx context.Context, t time.Time) (string, error)
}

type URL struct {
	collector PictureCollector
}

func NewURL(collector PictureCollector) *URL {
	return &URL{
		collector: collector,
	}
}

func (svc *URL) CollectPictures(ctx context.Context, dates internal.Dates) ([]string, error) {
	from := dates.From()
	// we must add 1 here, because without this one day will be skipped
	days := int(dates.To().Sub(from).Hours()/24) + 1

	type result struct {
		index int
		url   string
		err   error
	}

	resultsChan := make(chan result, days)
	defer close(resultsChan)
	var wg sync.WaitGroup
	for i := 0; i < days; i++ {
		wg.Add(1)

		go func(i int) {
			defer wg.Done()

			res := result{
				index: i,
			}
			res.url, res.err = svc.collector.Collect(ctx, from.Add(time.Duration(i)*time.Hour*24))
			resultsChan <- res
		}(i)
	}

	wg.Wait()

	urls := make([]string, days)
	for res := range resultsChan {
		if res.err != nil {
			return nil, res.err
		}

		urls[res.index] = res.url

		if len(resultsChan) == 0 {
			break
		}
	}

	return urls, nil
}
