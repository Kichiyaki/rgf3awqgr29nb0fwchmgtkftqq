package internal

import "fmt"

type ErrorCode uint

const (
	ErrorCodeUnknown ErrorCode = iota
	ErrorCodeBadArgument
	ErrorCodeMethodNotAllowed
	ErrorCodeTooManyRequests
)

// String translates the error code into a human-readable message.
func (code ErrorCode) String() string {
	switch code {
	case ErrorCodeBadArgument:
		return "invalid payload"
	case ErrorCodeMethodNotAllowed:
		return "method not allowed"
	case ErrorCodeTooManyRequests:
		return "rate limit has been exceeded"
	case ErrorCodeUnknown:
		fallthrough
	default:
		return "internal server error"
	}
}

// Error implements the error interface.
// In addition, it has a message, an error code and the original error (on condition that the WrapError/WrapErrorf method was used).
type Error struct {
	orig error
	msg  string
	code ErrorCode
}

// NewError returns an error with the supplied message and error code.
func NewError(code ErrorCode, msg string) error {
	return WrapError(nil, code, msg)
}

// NewErrorf formats according to a format specifier and returns the string
// as a value that satisfies error.
func NewErrorf(code ErrorCode, format string, a ...interface{}) error {
	return WrapErrorf(nil, code, format, a...)
}

// WrapError returns a new error with the supplied message, the error code and the original error.
func WrapError(orig error, code ErrorCode, msg string) error {
	return &Error{
		orig: orig,
		code: code,
		msg:  msg,
	}
}

// WrapErrorf returns a new error with the formatted message, the error code and the original error.
func WrapErrorf(orig error, code ErrorCode, format string, a ...interface{}) error {
	return &Error{
		orig: orig,
		code: code,
		msg:  fmt.Sprintf(format, a...),
	}
}

func (e *Error) Error() string {
	if e.orig == nil {
		return e.msg
	}

	return e.msg + ": " + e.orig.Error()
}

func (e *Error) Unwrap() error {
	return e.orig
}

func (e *Error) Code() ErrorCode {
	return e.code
}

// GetErrorCode returns an error code that is stored in the supplied error.
// If the error has a type other than Error, ErrorCodeUnknown is returned.
func GetErrorCode(err error) ErrorCode {
	ierr, ok := err.(interface {
		Code() ErrorCode
	})
	if !ok {
		return ErrorCodeUnknown
	}

	return ierr.Code()
}
