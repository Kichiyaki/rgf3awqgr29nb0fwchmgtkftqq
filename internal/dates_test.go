package internal_test

import (
	"testing"
	"time"

	"gitlab.com/Kichiyaki/rgf3awqgr29nb0fwchmgtkftqq/internal"
)

func TestNewDates(t *testing.T) {
	t.Parallel()

	type dates struct {
		from time.Time
		to   time.Time
	}

	defaultFormatter := func(t time.Time) string {
		return t.Format("2006-01-02")
	}

	type formatters struct {
		from func(t time.Time) string
		to   func(t time.Time) string
	}

	defaultFormatters := formatters{
		from: defaultFormatter,
		to:   defaultFormatter,
	}

	tests := []struct {
		name       string
		dates      dates
		formatters formatters
		err        error
	}{
		{
			name:       "OK",
			formatters: defaultFormatters,
			dates: dates{
				from: time.Now().Add(-48 * time.Hour),
				to:   time.Now(),
			},
		},
		{
			name: "ERR: from - invalid format",
			formatters: formatters{
				from: func(t time.Time) string {
					return t.Format(time.RFC3339)
				},
				to: defaultFormatter,
			},
			dates: dates{
				from: time.Now().Add(-48 * time.Hour),
				to:   time.Now(),
			},
			err: internal.ErrFromInvalidTimeFormat,
		},
		{
			name: "ERR: to - invalid format",
			formatters: formatters{
				from: defaultFormatter,
				to: func(t time.Time) string {
					return t.Format(time.RFC3339)
				},
			},
			dates: dates{
				from: time.Now().Add(-48 * time.Hour),
				to:   time.Now(),
			},
			err: internal.ErrToInvalidTimeFormat,
		},
		{
			name:       "ERR: `from` should be earlier than `to`",
			formatters: defaultFormatters,
			dates: dates{
				from: time.Now(),
				to:   time.Now().Add(-48 * time.Hour),
			},
			err: internal.ErrFromShouldBeEarlierThanTo,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			d, err := internal.NewDates(tt.formatters.from(tt.dates.from), tt.formatters.to(tt.dates.to))
			if tt.err != err {
				t.Errorf("expected different error, got %v, expected %v", err, tt.err)
			}
			if err != nil {
				return
			}

			if tt.formatters.from(tt.dates.from) != tt.formatters.from(d.From()) {
				t.Errorf(
					"returned invalid `from` value, got %s, expected %s",
					tt.formatters.from(d.From()),
					tt.formatters.from(tt.dates.from),
				)
			}

			if tt.formatters.to(tt.dates.to) != tt.formatters.to(d.To()) {
				t.Errorf(
					"returned invalid `to` value, got %s, expected %s",
					tt.formatters.to(d.To()),
					tt.formatters.to(tt.dates.to),
				)
			}
		})
	}
}
