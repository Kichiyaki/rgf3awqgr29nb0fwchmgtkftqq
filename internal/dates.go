package internal

import "time"

var (
	ErrFromInvalidTimeFormat     = NewError(ErrorCodeBadArgument, "`from`: invalid date format, expected YYYY-MM-DD")
	ErrToInvalidTimeFormat       = NewError(ErrorCodeBadArgument, "`to`: invalid date format, expected YYYY-MM-DD")
	ErrFromShouldBeEarlierThanTo = NewError(ErrorCodeBadArgument, "`from` should be earlier than to")
)

type Dates struct {
	from time.Time
	to   time.Time
}

func NewDates(from, to string) (Dates, error) {
	layout := "2006-01-02"

	fromT, err := time.Parse(layout, from)
	if err != nil {
		return Dates{}, ErrFromInvalidTimeFormat
	}

	toT, err := time.Parse(layout, to)
	if err != nil {
		return Dates{}, ErrToInvalidTimeFormat
	}

	if toT.Before(fromT) {
		return Dates{}, ErrFromShouldBeEarlierThanTo
	}

	return Dates{
		from: fromT,
		to:   toT,
	}, nil
}

func (d Dates) From() time.Time {
	return d.from
}

func (d Dates) To() time.Time {
	return d.to
}
