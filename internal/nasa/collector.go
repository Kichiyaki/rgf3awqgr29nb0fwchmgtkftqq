package nasa

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"time"

	"gitlab.com/Kichiyaki/rgf3awqgr29nb0fwchmgtkftqq/internal"
)

const (
	host           = "api.nasa.gov"
	apodPath       = "planetary/apod"
	defaultTimeout = 10 * time.Second
	dateFormat     = "2006-01-02"
)

type Collector struct {
	apiKey string
	// this buffered channel will block at the concurrency limit
	semaphore chan struct{}
	client    *http.Client
}

func NewCollector(apiKey string, concurrentRequests int) *Collector {
	return &Collector{
		apiKey:    apiKey,
		semaphore: make(chan struct{}, concurrentRequests),
		client: &http.Client{
			Timeout: defaultTimeout,
		},
	}
}

type planetaryResp struct {
	URL string `json:"url"`
}

func (c *Collector) Collect(ctx context.Context, t time.Time) (string, error) {
	u := url.URL{
		Scheme: "https",
		Host:   host,
		Path:   apodPath,
	}
	q := u.Query()
	date := t.Format(dateFormat)
	q.Add("date", date)
	q.Add("api_key", c.apiKey)
	u.RawQuery = q.Encode()

	resp, err := c.get(ctx, u.String())
	if err != nil {
		return "", internal.WrapErrorf(err, internal.ErrorCodeUnknown, "couldn't fetch data for %s", date)
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != http.StatusOK {
		_, _ = io.Copy(io.Discard, resp.Body)

		if resp.StatusCode == http.StatusTooManyRequests {
			return "", internal.NewError(internal.ErrorCodeTooManyRequests, "rate limit has been exceeded")
		}

		return "", internal.NewErrorf(
			internal.ErrorCodeUnknown,
			"the NASA's APOD API returned different http status than %d for %s, got %d",
			http.StatusOK,
			date,
			resp.StatusCode,
		)
	}

	var respBody planetaryResp
	if err := json.NewDecoder(resp.Body).Decode(&respBody); err != nil {
		return "", internal.WrapErrorf(err, internal.ErrorCodeUnknown, "couldn't decode response body for %s", date)
	}

	return respBody.URL, nil
}

func (c *Collector) get(ctx context.Context, url string) (*http.Response, error) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	return c.do(req.WithContext(ctx))
}

func (c *Collector) do(req *http.Request) (*http.Response, error) {
	c.semaphore <- struct{}{}
	defer func() {
		<-c.semaphore
	}()

	return c.client.Do(req)
}

func (c *Collector) Close() {
	close(c.semaphore)
}
