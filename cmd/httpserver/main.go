package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"gitlab.com/Kichiyaki/rgf3awqgr29nb0fwchmgtkftqq/internal/nasa"
	"gitlab.com/Kichiyaki/rgf3awqgr29nb0fwchmgtkftqq/internal/rest"
	"gitlab.com/Kichiyaki/rgf3awqgr29nb0fwchmgtkftqq/internal/service"

	"gitlab.com/Kichiyaki/rgf3awqgr29nb0fwchmgtkftqq/internal"
)

const (
	defaultPort               = "8080"
	defaultConcurrentRequests = 5
	defaultAPIKey             = "DEMO_KEY"
)

func main() {
	defer func() {
		log.Println("shutdown completed")
	}()

	concurrentRequests, err := getConcurrentRequests()
	if err != nil {
		log.Fatalln("internal.GetConcurrentRequests", err)
	}

	apiKey := getAPIKey()

	collector := nasa.NewCollector(apiKey, concurrentRequests)
	defer collector.Close()

	svc := service.NewURL(collector)

	mux := http.NewServeMux()
	rest.NewURLHandler(svc).Register(mux)

	port := getPort()

	srv := &http.Server{
		Addr:              ":" + port,
		Handler:           mux,
		ReadTimeout:       2 * time.Second,
		ReadHeaderTimeout: 2 * time.Second,
		WriteTimeout:      10 * time.Second,
		IdleTimeout:       2 * time.Second,
	}

	go func(srv *http.Server) {
		ctx, stop := signal.NotifyContext(context.Background(),
			os.Interrupt,
			syscall.SIGTERM,
			syscall.SIGQUIT)
		defer stop()

		<-ctx.Done()

		log.Println("shutdown signal received, exiting...")

		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		if err := srv.Shutdown(ctx); err != nil {
			log.Fatalln("srv.Shutdown", err)
		}
	}(srv)

	log.Println("server is listening on the port " + port)

	if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatalln("ListenAndServe", err.Error())
	}
}

func getPort() string {
	port := os.Getenv("PORT")
	if port == "" {
		return defaultPort
	}

	return port
}

func getAPIKey() string {
	apiKey := os.Getenv("API_KEY")
	if apiKey == "" {
		return defaultAPIKey
	}

	return apiKey
}

func getConcurrentRequests() (int, error) {
	concurrentRequests := os.Getenv("CONCURRENT_REQUESTS")
	if concurrentRequests == "" {
		return defaultConcurrentRequests, nil
	}

	concurrentRequestsInt, err := strconv.Atoi(concurrentRequests)
	if err != nil {
		return 0, internal.WrapError(err, internal.ErrorCodeUnknown, "invalid value for the env 'CONCURRENT_REQUESTS'")
	}

	return concurrentRequestsInt, nil
}
